const e = require("express");
const express = require("express");

const app = express();
// khai báo chạy trên cổng 8000
const prot = 8000;
// khai báo để app đọc được body json
app.use(express.json());

app.get("/",(req,res)=>{
    let day = new Date();

    res.json({
        message: `xin chào hôm nay là ngày ${day.getDate()} tháng ${day.getMonth()+1} năm ${day.getFullYear()}`
    })
})




// request Params (get,put,delete)
app.get("/request-param/:param1/:param2",(req,res)=>{
    let param1 = req.params.param1;
    let param2 = req.params.param2;

    res.json ({
        params: {
            param1,
            param2
        }
    })
})

// request Query ( chỉ ap dụng cho phương thức get)
// Khi lấy query bắt buộc phải valide (kiểm tra)
app.get("/request-query", (req,res)=>{
    let query = req.query;
    res.json ({
        query
    })
})

// request body json ( chỉ áp dụng cho phương thức put và post)
// Khi lấy query bắt buộc phải valide (kiểm tra)
// Cần khao báo thêm câu lệnh để đọc được bodyjson
app.post ("/request-bodyjson",(req,res)=>{
    let body = req.body;

    res.json ({
        body
    })
})


// // Khai báo API dạng get
// app.get("/get-method",(req,res)=>{
//     res.json({
//         message:"get method"
//     })
// })

// // Khai báo API dạng POST
// app.post("/post-method",(req,res)=>{
//     res.json({
//         message:"post method"
//     })
// })

// // Khai báo API dạng PUT
// app.put("/put-method",(req,res)=>{
//     res.json({
//         message:"put method"
//     })
// })

// //Khai báo API dạng DELETE
// app.delete("/delete-method", (req,res)=>{
//     res.json({
//         message:"delete method"
//     })
// })

app.listen(prot, ()=>{
    console.log("app listen on prot", prot)
});
